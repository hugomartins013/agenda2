package Test;

import static org.junit.Assert.*;

import Model.Pessoa;

import org.junit.Test;

public class PessoaTest {

	@Test
	public void testNome() {
		
		Pessoa pessoaTest = new Pessoa();
		
		pessoaTest.setNome("Hugo");
		
		assertEquals("Hugo", pessoaTest.getNome());
	}
	
	public void testSobreNome() {
		
		Pessoa pessoaTest = new Pessoa();
		
		pessoaTest.setSobreNome("Martins");
		
		assertEquals("Martins", pessoaTest.getSobreNome());
	}
	
	public void testTelefone() {
		
		Pessoa pessoaTest = new Pessoa();
		
		pessoaTest.setTelefone("78956412");
		
		assertEquals("78956412", pessoaTest.getTelefone());
	}
	
	public void testEmail(){
		
		Pessoa pessoaTest = new Pessoa();
		
		pessoaTest.setEmail("hugo@martins");
		
		assertEquals("hugo@martins", pessoaTest.getEmail());
	}
	
	public void testIdade(){
		
		Pessoa pessoaTest = new Pessoa();
		
		pessoaTest.setIdade("21");
		
		assertEquals("21", pessoaTest.getIdade());
	}
	
	public void testSexo(){
		
		Pessoa pessoaTest = new Pessoa();
		
		pessoaTest.setSexo("M");
		
		assertEquals("M", pessoaTest.getSexo());
	}
	
	public void testHangout(){
		
		Pessoa pessoaTest = new Pessoa();
		
		pessoaTest.setHangout("hugo@martins");
		
		assertEquals("hugo@martins", pessoaTest.getHangout());
	}
	
	public void testEndereco(){
		
		Pessoa pessoaTest = new Pessoa();
		
		pessoaTest.setEndereco("Avenida dos desesperados, rua da margura nº 23");
		
		assertEquals("Avenida dos desesperados, rua da margura nº 23", pessoaTest.getEndereco());
	}
	
	public void testRg(){
		
		Pessoa pessoaTest = new Pessoa();
		
		pessoaTest.setRg("2345678");
		
		assertEquals("2345678", pessoaTest.getRg());
	}
	
	public void testCpf(){
		
		Pessoa pessoaTest = new Pessoa();
		
		pessoaTest.setCpf("234567890");
		
		assertEquals("234567890", pessoaTest.getCpf());
	}

}
