package Test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import Model.Pessoa;
import Control.ControlePessoa;

public class ControlePessoaTest {
	
	private ControlePessoa umControlePessoa;
	private Pessoa umaPessoa;
		
		@Before
		public void setUp()throws Exception{
			umControlePessoa = new ControlePessoa();
			umaPessoa = new Pessoa();
		}

		@Test
		public void testAdicionar() {		
			assertEquals("Pessoa adicionada com Sucesso!", umControlePessoa.adicionar(umaPessoa));
			
		}
		
		@Test
		public void testRemover() {		
			assertEquals("Pessoa removida com Sucesso!", umControlePessoa.remover(umaPessoa));
			
		}
		
		@Test
		public void testPesquisar() {
			String umNome = "Hugo";
			umaPessoa.setNome(umNome);
			umControlePessoa.adicionar(umaPessoa);
			
			assertEquals(umaPessoa, umControlePessoa.pesquisarNome(umNome));
			
		}

}
