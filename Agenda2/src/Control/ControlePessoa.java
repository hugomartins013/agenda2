package Control;
 import Model.Pessoa;
 import java.util.ArrayList;


public class ControlePessoa {


	//atributos

	    private ArrayList<Pessoa> listaPessoas;

	//construtor

	    public ControlePessoa() {
	        listaPessoas = new ArrayList<Pessoa>();
	    }

	// métodos
	   
	    public String adicionar(Pessoa umaPessoa) {
	        String mensagem = "Pessoa adicionada com Sucesso!";
			listaPessoas.add(umaPessoa);
			return mensagem;
	    }

	    public String remover(Pessoa umaPessoa) {
	        String mensagem = "Pessoa removida com Sucesso!";
	        listaPessoas.remove(umaPessoa);
	        return mensagem;
	    }
	    
	    public Pessoa pesquisarNome(String umNome) {
	        for (Pessoa umaPessoa: listaPessoas) {
	            if (umaPessoa.getNome().equalsIgnoreCase(umNome)) return umaPessoa;
	        }
	        return null;
	    }

	    public Pessoa pesquisarTelefone (String umTelefone) {
	        for (Pessoa umaPessoa : listaPessoas) {
	            if (umaPessoa.getTelefone().equals(umTelefone)) return umaPessoa;
	        }
	        return null;
	    }

	    public void exibirContatos () {
}
}